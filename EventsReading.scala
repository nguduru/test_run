import org.apache.commons.lang.exception.ExceptionUtils.getStackTrace
import org.apache.log4j.{Level, Logger}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.sql.types.{StringType, StructField, StructType}
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import scala.collection._

case class Event(event_id: String, collector_tstamp: String, domain_userid: String, page_urlpath: String, next_event_id: String){
  override def toString: String = s"$event_id, $collector_tstamp, $domain_userid, $page_urlpath, $next_event_id"
}
object EventsReading {
  
  def main(args: Array[String]): Unit = {

    setLogLevels(Level.OFF, Seq("spark", "org", "akka", "hive"))

    val sc: SparkContext = getSparkContext("local", "WebTrafficEventsReading")

    val sqlContext: SQLContext = new HiveContext(sc)
    import sqlContext.implicits._

    val lagVaue = lag('event_id, 1).over(Window.partitionBy("domain_userid"))

    val csvData = pullDataFromCSVFile(sqlContext, true, "C:\\Users\\naga\\Downloads\\pageViews.csv", null, null).withColumn("next_event_id", lagVaue).as[Event]
    println("Printing top 150 records")
    csvData.show(150)
    csvData.printSchema()


    val csvDataWithNextEventIdAsList = csvData.collectAsList.asScala.toList

    println("Printing LIST OBJECT is ::::::::::::::: \n "+csvDataWithNextEventIdAsList.mkString("\n"))

  }

  def getSparkContext(runLocal: String, appName: String): SparkContext = {
    val sc = if (runLocal.equalsIgnoreCase("local") || runLocal.equalsIgnoreCase("l")) {
      val sparkConfig = new SparkConf()
      new SparkContext("local[*]", appName, sparkConfig)
    } else {
      val sparkConfig = new SparkConf().setAppName(appName)
      SparkContext.getOrCreate(sparkConfig)
    }
    sc.hadoopConfiguration.setBoolean("parquet.enable.summary-metadata", false)
    sc
  }

  def pullDataFromCSVFile(sqlContext: SQLContext, isHeaderExist: Boolean, filePath: String, delimiter: String, csvSplit: String): DataFrame = {

    def createSchemaFromStrDelValues(baseSchema: String): StructType = {
      StructType(baseSchema.split(",").map(f => StructField(f, StringType, true)))
    }

    var csvDataFrame: DataFrame = null
    try {
      if (isHeaderExist) {
        csvDataFrame = sqlContext.read
          .format("com.databricks.spark.csv")
          .option("header", "true")
          .option("inferSchema", "true")
          .load(filePath)
      } else {
        if (csvSplit != null) {
          val schema = createSchemaFromStrDelValues(csvSplit)
          csvDataFrame = sqlContext.read
            .format("com.databricks.spark.csv")
            .option("header", "false")
            .option("delimiter", delimiter)
            .option("inferSchema", "false")
            .schema(schema)
            .load(filePath)
        }
      }
    } catch {
      case ex: Exception =>
        Console.err.println(s"Unable to read the CSV file from the location $filePath ${getStackTrace(ex)}")
        throw ex
    }
    csvDataFrame
  }

  def setLogLevels(level: Level, loggers: Seq[String]): Unit = {
    loggers.foreach(loggerName => Logger.getLogger(loggerName).setLevel(level))
    println(s"Running the application in $level")
  }

}